# Genocation.xyz

Server and SSL docker-compose for genocation.xyz

Guide at:

https://github.com/wmnnd/nginx-certbot/

# Restart home
```
cd ../home/
git pull origin master
docker-compose up -d --no-deps --build home
```

# Instructions

To add new domains:

* Update default.cert.conf and default.nocert.conf
* Replace data/nginx/default.conf with default.nocert.conf
* Run ./init-letsencrypt.sh
* Once the certificates are created, replace data/nginx/default.conf with default.cert.conf
* Reinitialize with `docker-compose up -d`
* Or restart docker-compose nginx `docker exec nginx nginx -s reload`


# Boilerplate for nginx with Let’s Encrypt on docker-compose

> This repository is accompanied by a [step-by-step guide on how to set up nginx and Let’s Encrypt
  with
  Docker](https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71).

`init-letsencrypt.sh` fetches and ensures the renewal of a Let’s Encrypt certificate for one or
multiple domains in a docker-compose setup with nginx.  This is useful when you need to set up nginx
as a reverse proxy for an application.

## Installation
1. [Install docker-compose](https://docs.docker.com/compose/install/#install-compose).

2. Clone this repository: `git clone https://github.com/wmnnd/nginx-certbot.git .`

3. Modify configuration:
- Add domains and email addresses to init-letsencrypt.sh
- Replace all occurrences of example.org with primary domain (the first one you added to
  init-letsencrypt.sh) in data/nginx/app.conf

4. Run the init script:

        ./init-letsencrypt.sh

5. Run the server:

        docker-compose up

## Got questions?  Feel free to post questions in the comment section of the [accompanying
guide](https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71)

## License All code in this repository is licensed under the terms of the `MIT License`. For further
information please refer to the `LICENSE` file.
